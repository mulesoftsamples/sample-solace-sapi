# Solace Service Mesh - Sample Application #

This is a sample application created to demonstrate how to integrate [Solace Service Mesh](http://www.solace.com) with Mulesoft 4.2.2.

### Mule App Arguments ###
```bash
-M-XX:-UseBiasedLocking -M-Dfile.encoding=UTF-8 -M-XX:+UseG1GC -M-XX:+UseStringDeduplication -M-Dmule.env=dev
```

### Supported Protocols ###

* JMS
* HTTP (REST) - *Under revision**

### JMS ###


* POM Required Dependencies
```xml
    	<dependency>
            <groupId>com.solacesystems</groupId>
            <artifactId>sol-jcsmp</artifactId>
            <version>10.2.0</version>
        </dependency>
    	<dependency>
            <groupId>com.solacesystems</groupId>
            <artifactId>sol-jms</artifactId>
            <version>10.3.0</version>
        </dependency>
```